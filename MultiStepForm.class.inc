<?php

class MultiStepForm {
	private $module;
	private $module_path;
	private $path;
	private $form_name;
	private $step;

	function __construct($form, &$form_state, $options) {
			foreach($options as $key => $value) {
				switch($key) {
					case 'module':
					case 'path':
					case 'form_name':
						$this->{$key} = $value;
					break;
				}
			}
			$this->module_path = drupal_get_path('module', $this->module);
	    $this->steps = $this->get_form_steps();

	  // Initialize a description of the steps for the wizard.
	  if(empty($form_state['step'])) {



	    $form_state['step'] = 1;

	    //dsm($this->steps);	
	    // This array contains the function to be called at each step to get the
	    // relevant form elements. It will also store state information for each
	    // step.

	    $form_state['steps'] = $this->steps;
	  }

	  $this->step = &$form_state['step'];
	  //dsm($this->steps);
	  if(!$step = $this->get_step($this->step)) {
			$this->form = $form;
			dsm($form_state);
			return;
	  	$this->error(t('Step @step does not exist', array('@step' => $this->step)), 'fatal');
	  }

	  //dsm($form_state);


	  form_load_include($form_state, $step->extension, $this->module, $this->path . '/' . $step->basename);
	  require_once($step->dirname . '/' . $step->basename);
	  $form_name = $this->module . '_' . $step->form_name;
	  $this->form = $form_name($form, $form_state);

	  drupal_set_title(t('@title: Step @step of @steps', array('@title' => menu_get_active_title(), '@step' => $this->step, '@steps' => count($this->steps))));

	
	  // Show the 'previous' button if appropriate. Note that #submit is set to
	  // a special submit handler, and that we use #limit_validation_errors to
	  // skip all complaints about validation when using the back button. The
	  // values entered will be discarded, but they will not be validated, which
	  // would be annoying in a "back" button.
	  if ($this->step > 1) {
	    $this->form['prev'] = array(
	      '#type' => 'submit',
	      '#value' => t('Previous'),
	      '#name' => 'prev',
	      '#submit' => array('multistepform_previous_submit'),
	      '#limit_validation_errors' => array(),
	    );
	  }
	
	  // Show the Next button only if there are more steps defined.
	  if ($this->step < count($this->steps)) {
	    // The Next button should be included on every step
	    $this->form['next'] = array(
	      '#type' => 'submit',
	      '#value' => t('Next'),
	      '#name' => 'next',
	      '#submit' => array('multistepform_next_submit'),
	    );
	  }
	  else {
	    // Just in case there are no more steps, we use the default submit handler of
	    // the form wizard. Call this button Finish, Submit, or whatever you want to
	    // show. When this button is clicked, the multistepform_submit handler
	    // will be called.
	    $this->form['finish'] = array(
	      '#type' => 'submit',
	      '#value' => t('Finish'),
	    );
	  }

	  $form_name = $this->module . '_' . $this->form_name . '_step_' . $this->step;
	  if(function_exists($form_name . '_validate')) {
	  	$this->form['next']['#validate'][] = $form_name . '_validate';
	  }
	  if(function_exists($form_name . '_submit')) {
	  	$this->form['next']['#submit'][] = $form_name . '_submit';
	  }
	}

	private function error($error, $type) {
		switch($type) {
			case 'fatal':
				throw new Exception($error);
			break;
		}
	}

	public function get_step($step) {
		if(isset($this->steps[$step])) {
			return $this->steps[$step];
		} else {
			return FALSE;
		}
	}


	public function get_form_steps() {
 		return multistepform_get_steps($this->module_path, $this->path, $this->form_name);
	}

	public function form() {
		return $this->form;
	}

}
